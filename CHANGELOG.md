# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 1.0.1 (2022-03-09)

## [1.0.0] - 2021-09-07
### Added
- create CHANGELOG.md

### Fixed
- fix test, ORM eager load was accepts empty with array.
- fix test, ORM snapshot state was public access, now require accessor to read it.