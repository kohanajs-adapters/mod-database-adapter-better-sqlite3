const { ORMAdapter } = require('kohanajs');

class ORMAdapterSQLite extends ORMAdapter {
  static OP = ({
    ...ORMAdapter.OP,
    NOT_EQUAL: '!=',
    TRUE: 1,
    FALSE: 0,
  })

  static op(operator, placeHolder = false) {
    if (operator === '') return '';
    if (placeHolder) return '?';
    const OP = ORMAdapterSQLite.OP[operator];
    if (OP === undefined) {
      return (typeof operator === 'string') ? `'${operator}'` : operator;
    }

    return OP;
  }

  static formatCriteria(criteria) {
    if (!Array.isArray(criteria[0])) throw new Error('criteria must group by array.');
    return criteria.map(
      (x, i) => `${(i === 0) ? '' : this.op(x[0] || '')} ${x[1] || ''} ${this.op(x[2] || '')} ${this.op(x[3] || '', true)} `,
    );
  }

  static translateValue(values) {
    return values.map(x => {
      if (x === null) return null;
      if (typeof x === 'boolean') return x ? 1 : 0;
      if (typeof x === 'object') return JSON.stringify(x);
      if (typeof x === 'function') return JSON.stringify(x);
      return x;
    });
  }

  async read() {
    return this.database.prepare(`SELECT * from ${this.tableName} WHERE id = ?`).get(this.client.id);
  }

  async update(values) {
    const columns = this.client.getColumns();
    this.database.prepare(`UPDATE ${this.tableName} SET ${columns.map(x => `${x} = ?`).join(', ')} WHERE id = ?`).run(...values, this.client.id);
  }

  async insert(values) {
    const columns = this.client.getColumns();
    const sql = `INSERT OR FAIL INTO ${this.tableName} (${columns.join(', ')}, id) VALUES (?, ${columns.map(x => '?').join(', ')})`;
    this.database.prepare(sql).run(...values, this.client.id);
  }

  async delete() {
    this.database.prepare(`DELETE FROM ${this.tableName} WHERE id = ?`).run(this.client.id);
  }

  async hasMany(tableName, key) {
    return this.database.prepare(`SELECT * FROM ${tableName} WHERE ${key} = ?`).all(this.client.id);
  }

  async belongsToMany(modelTableName, jointTableName, lk, fk) {
    return this.database.prepare(
      `SELECT ${modelTableName}.* FROM ${modelTableName} JOIN ${jointTableName} ON ${modelTableName}.id = ${jointTableName}.${fk} `
      + `WHERE ${jointTableName}.${lk} = ? ORDER BY ${jointTableName}.weight`,
    ).all(this.client.id);
  }

  async add(models, weight, jointTableName, lk, fk) {
    const ids = models.map(x => x.id);
    const values = models.map((x, i) => `(${this.client.id} , ?, ${weight + (i * 0.000001)})`);
    this.database.prepare(`INSERT OR IGNORE INTO ${jointTableName} (${lk}, ${fk}, weight) VALUES ${values.join(', ')}`).run(...ids);
  }

  async remove(models, jointTableName, lk, fk) {
    const ids = models.map(x => x.id);
    const sql = `DELETE FROM ${jointTableName} WHERE ${lk} = ${this.client.id} AND ${fk} IN (${ids.map(() => '?').join(', ')})`;
    this.database.prepare(sql).run(...ids);
  }

  async removeAll(jointTableName, lk) {
    this.database.prepare(`DELETE FROM ${jointTableName} WHERE ${lk} = ?`).run(this.client.id);
  }

  async readResult(limit, sql, values) {
    const statement = this.database.prepare(sql);

    if (limit === 1) {
      const result = statement.get(...values);
      if (!result) return [];
      return [result];
    }
    return statement.all(...values);
  }

  async readAll(kv, limit = 1000, offset = 0, orderBy = new Map([['id', 'ASC']])) {
    const statementOrderBy = ORMAdapterSQLite.getOrderByStatement(orderBy);

    if (!kv) {
      return this.readResult(limit,
        `SELECT * FROM ${this.tableName} ${statementOrderBy} LIMIT ${limit} OFFSET ${offset}`,
        []);
    }

    return this.readResult(limit,
      `SELECT * FROM ${this.tableName} WHERE ${Array.from(kv.keys()).map(k => `${k} = ?`).join(' AND ')}${statementOrderBy} LIMIT ${limit} OFFSET ${offset}`,
      ORMAdapterSQLite.translateValue(Array.from(kv.values())));
  }

  async readBy(key, values, limit = 1000, offset = 0, orderBy = new Map([['id', 'ASC']])) {
    const statementOrderBy = ORMAdapterSQLite.getOrderByStatement(orderBy);
    return this.readResult(limit,
      `SELECT * FROM ${this.tableName} WHERE ${key} IN (${values.map(() => '?').join(', ')})${statementOrderBy} LIMIT ${limit} OFFSET ${offset}`,
      ORMAdapterSQLite.translateValue(values));
  }

  async readWith(criteria, limit = 1000, offset = 0, orderBy = new Map([['id', 'ASC']])) {
    const statementOrderBy = ORMAdapterSQLite.getOrderByStatement(orderBy);
    const { wheres, whereValues } = ORMAdapterSQLite.getWheresAndWhereValueFromCriteria(criteria);
    const sql = `SELECT * FROM ${this.tableName} WHERE ${wheres.join('')}${statementOrderBy} LIMIT ${limit} OFFSET ${offset}`;
    return this.readResult(limit, sql, whereValues);
  }

  static getOrderByStatement(orderBy) {
    return ` ORDER BY ${Array.from(orderBy).map(kv => `${kv[0]} ${kv[1]}`).join(',')}`;
  }

  async count(kv = null) {
    const where = kv ? ` WHERE ${Array.from(kv.keys()).map(k => `${k} = ?`).join(' AND ')}` : '';
    const v = kv ? ORMAdapterSQLite.translateValue(Array.from(kv.values())) : [];
    const result = this.database.prepare(`SELECT COUNT(id) FROM ${this.tableName}${where}`).get(...v);
    return result['COUNT(id)'];
  }

  async deleteAll(kv = null) {
    if (!kv) return this.database.prepare(`DELETE FROM ${this.tableName}`).run();
    const v = ORMAdapterSQLite.translateValue(Array.from(kv.values()));
    return this.database.prepare(`DELETE FROM ${this.tableName} WHERE ${Array.from(kv.keys()).map(k => `${k} = ?`).join(' AND ')}`).run(...v);
  }

  async deleteBy(key, values) {
    const v = ORMAdapterSQLite.translateValue(values);
    return this.database.prepare(`DELETE FROM ${this.tableName} WHERE ${key} IN (${v.map(() => '?').join(', ')})`).run(...v);
  }

  async deleteWith(criteria) {
    const { wheres, whereValues } = ORMAdapterSQLite.getWheresAndWhereValueFromCriteria(criteria);
    const sql = `DELETE FROM ${this.tableName} WHERE ${wheres.join('')}`;
    return this.database.prepare(sql).run(whereValues);
  }

  static getWheresAndWhereValueFromCriteria(criteria) {
    const wheres = this.formatCriteria(criteria);
    const whereValues = [];
    criteria.forEach(v => {
      let nv = v[3];
      if (nv === 'TRUE')nv = 1;
      if (nv === 'FALSE')nv = 0;
      if (nv === undefined) return;
      whereValues.push(nv);
    });

    return {
      wheres,
      whereValues,
    };
  }

  async updateAll(kv, columnValues) {
    const keys = Array.from(columnValues.keys());
    const newValues = ORMAdapterSQLite.translateValue(Array.from(columnValues.values()));
    if (!kv) return this.database.prepare(`UPDATE ${this.tableName} SET ${keys.map(key => `${key} = ?`)}`).run(...newValues);
    const v = ORMAdapterSQLite.translateValue(Array.from(kv.values()));
    return this.database.prepare(
      `UPDATE ${this.tableName} SET ${keys.map(key => `${key} = ?`)} WHERE ${Array.from(kv.keys()).map(k => `${k} = ?`).join(' AND ')}`,
    ).run(...newValues, ...v);
  }

  async updateBy(key, values, columnValues) {
    const keys = Array.from(columnValues.keys());
    const newValues = ORMAdapterSQLite.translateValue(Array.from(columnValues.values()));
    const v = ORMAdapterSQLite.translateValue(values);
    return this.database.prepare(`UPDATE ${this.tableName} SET ${keys.map(k => `${k} = ?`)} WHERE ${key} IN (${v.map(() => '?').join(', ')})`)
      .run(...newValues, ...v);
  }

  async updateWith(criteria, columnValues) {
    const keys = Array.from(columnValues.keys());
    const { wheres, whereValues } = ORMAdapterSQLite.getWheresAndWhereValueFromCriteria(criteria);

    const newValues = ORMAdapterSQLite.translateValue(Array.from(columnValues.values())).concat(whereValues);

    const sql = `UPDATE ${this.tableName} SET ${keys.map(key => `${key} = ?`)} WHERE ${wheres.join('')}`;
    return this.database.prepare(sql).run(...newValues);
  }

  async insertAll(columns, valueGroups, ids) {
    const sql = `INSERT INTO ${this.tableName} (${columns.join(', ')}, id) VALUES ${
      valueGroups.map(values => `(${values.map(() => '?').join(', ')
      }, ?)`).join(', ')}`;
    // valueGroup need to process, add id, translateValues
    const newValueGroups = valueGroups.map((values, i) => {
      const newValues = ORMAdapterSQLite.translateValue(values);
      newValues.push(ids[i] || ORMAdapterSQLite.defaultID());
      return newValues;
    });
    return this.database.prepare(sql).run(...newValueGroups.flat());
  }
}

module.exports = ORMAdapterSQLite;
